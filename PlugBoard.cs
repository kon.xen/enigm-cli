namespace enigma_machine;

public class PlugBoard
{
  public PlugBoard(Dictionary<char, char> wiring)
  {
    if (wiring.Count > 0)
    {
      SetTheBoard(wiring);
    }
  }

  private Dictionary<char, char> _board = new Dictionary<char, char>()
  {
    { 'A', 'a' }, { 'B', 'b' }, { 'C', 'c' }, { 'D', 'd' }, { 'E', 'e' },
    { 'F', 'f' }, { 'G', 'g' }, { 'H', 'h' }, { 'I', 'i' }, { 'J', 'j' },
    { 'K', 'k' }, { 'L', 'l' }, { 'M', 'm' }, { 'N', 'n' }, { 'O', 'o' },
    { 'P', 'p' }, { 'Q', 'q' }, { 'R', 'r' }, { 'S', 's' }, { 'T', 't' },
    { 'U', 'u' }, { 'V', 'v' }, { 'W', 'w' }, { 'X', 'x' }, { 'Y', 'y' },
    { 'Z', 'z' }
  };

  private void SetTheBoard(Dictionary<char, char> connections)
  {
    foreach (var connection in connections)
    {
      ConnectLetters(connection.Key, connection.Value);
    }
  }

  private void ConnectLetters(char plugA, char plugB)
  {
    _board[plugA] = plugB;
    _board[plugB] = plugA;
  }

  public char PassCharacterThrough(char theCharacter)
  {
    return _board.ContainsKey(theCharacter) ? _board[theCharacter] : theCharacter;
  }
}