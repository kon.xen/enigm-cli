namespace enigma_machine;

public class SetingsQuery
{
  public string reflector { get; set; }
  public List<string[]> rotors { get; set; }
}