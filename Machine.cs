using System.Xml.Serialization;
using enigma_machine;

namespace enigma_machine;

public class Machine
{
  public Machine(Dictionary<int, Rotor> rotors, Dictionary<char, char> plugBoardConnections)
  {
    this._plugBoard = new PlugBoard(plugBoardConnections);
    this._rotors = rotors;
    // Todo set the rotors on their starting positions
    foreach (var rotor in rotors)
    {
      this._wheelStartingPositions.Add(rotor.Value.StartingPos);
      this._wheelCurentPositions.Add(rotor.Value.StartingPos);
    }
  }

  private Dictionary<int, Rotor> _rotors;
  private PlugBoard _plugBoard;
  private List<int> _wheelStartingPositions;
  private List<int> _wheelCurentPositions;
  private char _character;

  public List<char> EncodeMessage(List<char> theMessage)
  {
    List<char> encryptedMessage = new List<char>();

    foreach (var letter in theMessage)
    {
      this._character = letter;
      PassToPlugBoard(this._character);
      PassToRotorsLeft(this._character);
      PassToReflector(this._character);
      PassToRotorsRight(this._character);
      TurnRotors();
      encryptedMessage.Add(this._character);
    }

    return encryptedMessage;
  }

  public void PassToPlugBoard(char letter)
  {
    //do something to passthrough;
    this._character = letter;
  }

  public void PassToRotorsLeft(char letter)
  {
    //Todo pass the character to the wheels right to left
    this._character = letter;
  }

  public void PassToRotorsRight(char letter)
  {
    //Todo pass the character to the wheels Left to right
    this._character = letter;
  }

  public void PassToReflector(char letter)
  {
    //Todo pass the character to the reflector;
    this._character = letter;
  }

  public void TurnRotors()
  {
    //do something to turn the wheel;
  }
}