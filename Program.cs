﻿// See https://aka.ms/new-console-template for more information

using System.ComponentModel.Design;
using enigma_machine;
using Newtonsoft.Json;

Console.WriteLine(" --- setting up ---");

var reflectorWiring = new Pins();
var plugBoardConnections = new Dictionary<char, char>();

var message = new List<char>();
var encryptedMessage = new List<char>();

var rotorData = GetRotorQueries("rotors.json");
var reflectorData = GetReflectorQueries("reflectors.json");
var machineSettings = GetMachineSettings("settings.json");
var rotorSettings = machineSettings[0].rotors;
var reflector = machineSettings[0].reflector;

var rotors = new Dictionary<int, Rotor>();

var _enigma_machine = new Machine(rotors, plugBoardConnections);

//Todo add a query service ?
List<RotorQuery> GetRotorQueries(string filename)
{
  //open the json file, parse it and set the variables
  List<RotorQuery> data =
    JsonConvert.DeserializeObject<List<RotorQuery>>(File.ReadAllText(filename)).ToList();
  return data;
}

List<ReflectorQuery> GetReflectorQueries(string filename)
{
  //open the json file, parse it and set the variables
  List<ReflectorQuery> data =
    JsonConvert.DeserializeObject<List<ReflectorQuery>>(File.ReadAllText(filename)).ToList();
  return data;
}

List<SetingsQuery> GetMachineSettings(string filename)
{
  //open the json file, parse it and set the variables
  List<SetingsQuery> data =
    JsonConvert.DeserializeObject<List<SetingsQuery>>(File.ReadAllText(filename)).ToList();
  return data;
}
//Todo is evident from the testing functions that an interface needs to be used.

void CreateRotors()
{
  var i = 0;
  foreach (string[] rotorSetting in rotorSettings)
  {
    foreach (var rotor in rotorData)
    {
      if (rotor.name == rotorSetting[0])
      {
        var newRotor = new Rotor(rotor.name, Int32.Parse(rotorSetting[1]), rotor.wiring, rotor.notch,
          rotor.turnover);
        rotors.Add(i + 1, newRotor);
        Console.WriteLine($"rotor {rotor.name} added");
      }
    }
    i += 1;
  }
}

void ImportMessage()
{
  //Todo open the txt file and parse the file in a list.
  //take spaces off and do sanitization
  // message.Add( character ); 
}

void ExportEncryptedMessage()
{
  // message = enigma_machine.EncodeMessage( message );
  //todo convert the list of characters to a string.
  //todo export to txt file the string;
}

CreateRotors();
Console.Write("--- All good ---");