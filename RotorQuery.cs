namespace enigma_machine;

public class RotorQuery
{
  public string name { get; set; }
  public string wiring { get; set; }
  public string notch { get; set; }
  public string turnover { get; set; }
}