namespace enigma_machine;

public class Rotor
{
  public Rotor(string name, int startingPosition, string wiring, string notch, string turnover)
  {
    Set_Pins(wiring);
    SetNotchPositions(notch);
    SetTurnoverPositions(turnover);
    StartingPos = startingPosition;
    Name = name;
  }

  public string Name { get; set; }
  public Pins _pins = new Pins();
  public int StartingPos { get; set; }
  public char[] NotchPositions { get; set; } = new char[2];
  public char[] TurnoverPositions { get; set; } = new char[2];
  public int CurentPos { get; set; }

  private void Set_Pins(string letters)
  {
    foreach (char c in letters)
    {
      _pins.wiring[letters.IndexOf(c) + 1] = c;
    }
  }

  private void SetNotchPositions(string notchString)
  {
    foreach (char n in notchString)
    {
      NotchPositions[notchString.IndexOf(n)] = n;
    }
  }

  private void SetTurnoverPositions(string turnover)
  {
    var i = 1;

    foreach (char n in turnover)
    {
      TurnoverPositions[i] = n;
      i += 1;
    }
  }

  public void PassLeft(char character)
  {
    //pass character to the left
  }

  public void PassRight(char character)
  {
    //pass character to the right
  }
}