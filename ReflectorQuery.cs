namespace enigma_machine;

public class ReflectorQuery
{
  public string name { get; set; }
  public string wiring { get; set; }
}