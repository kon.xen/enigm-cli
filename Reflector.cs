namespace enigma_machine;

public class Reflector
{
  public Reflector(char[] letters)
  {
    SetPins(letters);
  }

  private Pins reflectorPins = new Pins();

  private void SetPins(char[] letters)
  {
    for (var i = 0; i < letters.Length; i++)
    {
      reflectorPins.wiring[i + 1] = letters[i];
    }
  }

  public void Reflect(char letter)
  {
    //not sure how to yet.
  }
}